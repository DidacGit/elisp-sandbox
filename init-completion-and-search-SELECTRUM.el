;;; setup-completion-and-search.el --- My completion configuration

;;; Commentary:

;; My Ivy Mode configuration

;;; Code:

(require 'use-package)

(use-package minibuffer
    :config
  ;; (setq completion-cycle-threshold 3)
  ;; (setq completion-flex-nospace nil)
  ;; (setq completion-pcm-complete-word-inserts-delimiters t) ; don't treat SPC as delimiter
  ;; (setq completion-show-help nil)
  ;; (setq completion-styles '(partial-completion substring initials flex))
  ;; (setq completions-format 'vertical)   ; *Completions* buffer
  (setq enable-recursive-minibuffers t))

(use-package selectrum
    :straight t
    :hook (after-init . selectrum-mode)
    :config)

(straight-use-package
 '(selectrum-prescient :host github :repo "raxod502/prescient.el"
   :files ("selectrum-prescient.el")))

;; to make sorting and filtering more intelligent
(selectrum-prescient-mode +1)
(prescient-persist-mode +1)

;; (use-package prescient
;;     :straight (:files ("selectrum-prescient.el"))
;;     :hook (after-init . selectrum-prescient-mode))

;; (use-package selectrum-prescient
;; :hook (after-init . selectrum-prescient-mode)
;; )


(provide 'setup-completion-and-search)
;;; setup-completion-and-search.el ends here

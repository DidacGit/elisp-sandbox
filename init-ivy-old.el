;;; setup-completion-and-search.el --- My completion configuration

;;; Commentary:

;; My Ivy Mode configuration

;;; Code:

(require 'use-package)

(use-package ivy
    :straight t
    :delight
    :hook (after-init . ivy-mode)
    :config
    (setq enable-recursive-minibuffers t)
    (setq ivy-count-format "(%d/%d) ")
    (define-key ivy-switch-buffer-map (kbd "<tab>") 'ivy-done))

(use-package counsel
    :straight t
    :after ivy
    :delight
    :config

    (setq counsel-rg-base-command
          "rg -SHn --no-heading --color never --no-follow --hidden %s")
    (setq counsel-find-file-occur-cmd; TODO Simplify this
          "ls -a | grep -i -E '%s' | tr '\\n' '\\0' | xargs -0 ls -d --group-directories-first")

    (defun prot/counsel-fzf-rg-files (&optional input dir)
      "Run `fzf' in tandem with `ripgrep' to find files in the
present directory.  If invoked from inside a version-controlled
repository, then the corresponding root is used instead."
      (interactive)
      (let* ((process-environment
              (cons (concat "FZF_DEFAULT_COMMAND=rg -Sn --color never --files --no-follow --hidden")
                    process-environment))
             (vc (vc-root-dir)))
	(if dir
            (counsel-fzf input dir)
          (if (eq vc nil)
              (counsel-fzf input default-directory)
            (counsel-fzf input vc)))))

    (defun prot/counsel-fzf-dir (arg)
      "Specify root directory for `counsel-fzf'."
      (prot/counsel-fzf-rg-files ivy-text
				 (read-directory-name
                                  (concat (car (split-string counsel-fzf-cmd))
                                          " in directory: "))))

    (defun prot/counsel-rg-dir (arg)
      "Specify root directory for `counsel-rg'."
      (let ((current-prefix-arg '(4)))
	(counsel-rg ivy-text nil "")))

    ;; TODO generalise for all relevant file/buffer counsel-*?
    (defun prot/counsel-fzf-ace-window (arg)
      "Use `ace-window' on `prot/counsel-fzf-rg-files' candidate."
      (ace-window t)
      (let ((default-directory (if (eq (vc-root-dir) nil)
                                   counsel--fzf-dir
				 (vc-root-dir))))
	(if (> (length (aw-window-list)) 1)
            (find-file arg)
          (find-file-other-window arg))
	(balance-windows (current-buffer))))

    ;; Pass functions as appropriate Ivy actions (accessed via M-o)
    (ivy-add-actions
     'counsel-fzf
     '(("r" prot/counsel-fzf-dir "change root directory")
       ("g" prot/counsel-rg-dir "use ripgrep in root directory")
       ("a" prot/counsel-fzf-ace-window "ace-window switch")))

    (ivy-add-actions
     'counsel-rg
     '(("r" prot/counsel-rg-dir "change root directory")
       ("z" prot/counsel-fzf-dir "find file with fzf in root directory")))

    (ivy-add-actions
     'counsel-find-file
     '(("g" prot/counsel-rg-dir "use ripgrep in root directory")
       ("z" prot/counsel-fzf-dir "find file with fzf in root directory")))


    (counsel-mode t)
    (setq counsel-yank-pop-separator "\n————————————————————————————————————\n")
    :bind (("C-x C-f" . counsel-find-file)
	   ("s-f" . counsel-find-file)
	   ("s-F" . find-file-other-window)
	   ("C-x b" . counsel-switch-buffer)
	   ("s-b" . counsel-switch-buffer)
	   ("<M-tab>" . counsel-switch-buffer)
	   ("C-x B" . counsel-switch-buffer-other-window)
	   ("s-B" . counsel-switch-buffer-other-window)
	   ;; ("C-x C-r" . counsel-recentf)
	   ("s-r" . counsel-recentf)
	   ("M-`" . counsel-imenu)
	   ;; ("s-m" . counsel-mark-ring)
	   ;; ("s-y" . counsel-yank-pop)
	   ("M-s r" . counsel-rg)
	   ("M-s g" . counsel-git-grep)
	   ("M-s l" . counsel-find-library)
	   ("M-s f" . prot/counsel-fzf-rg-files)))

(use-package swiper
    :straight t
    :after ivy
    ;; call swiper for small buffers and grep for large ones
    :bind (([remap occur] . counsel-grep-or-swiper)
	   :map isearch-mode-map
	   ([remap isearch-occur] . swiper-from-isearch)
	   :map swiper-map
	   ("M-s o" . ivy-occur)))

(use-package ivy-rich
    :straight t
    :after (ivy all-the-icons)
    :config
    ;; Use the 'all-the-icons' API for some of the transformers
    (defun ivy-rich-switch-buffer-icon (candidate)
      (with-current-buffer (get-buffer candidate)
	(let ((icon (all-the-icons-icon-for-mode major-mode)))
	  (if (symbolp icon)
	      (all-the-icons-icon-for-mode 'fundamental-mode)
	    icon))))
    ;; Add a counsel-switch-buffer transformer to the list of transformers
    (setq ivy-rich-display-transformers-list-old ivy-rich-display-transformers-list)
    (setq ivy-rich-display-transformers-list
	  (cons 'counsel-switch-buffer
		(cons '(:columns
			((ivy-rich-switch-buffer-icon (:width 5))
			 ;; (ivy-rich-candidate (:width 30))
			 ;; (ivy-rich-switch-buffer-size (:width 7))
			 ;; (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
			 (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
			 ;; (ivy-rich-switch-buffer-project (:width 15 :face success))
			 (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
			:predicate
			(lambda (cand) (get-buffer cand)))
		      ivy-rich-display-transformers-list-old)))
    (ivy-rich-mode 1)
    (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))

(use-package amx
    :straight t
    :config
    (amx-mode t))

(provide 'setup-completion-and-search)
;;; setup-completion-and-search.el ends here

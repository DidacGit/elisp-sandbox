;;; setup-exwm.el --- My EXWM configuration

;;; Commentary:

;; My EXWM configuration

;;; Code:

(require 'use-package)

(defun my-screenshot (num)
  (interactive "p")
  (if (= num 4)
      (start-process-shell-command "screenshot-alternative.sh" nil "screenshot-alternative.sh")
    (start-process-shell-command "screenshot.sh" nil "screenshot.sh")))
(defun my-volume-up ()
  (interactive)
  (start-process-shell-command "amixer set Master 2%+" nil "amixer set Master 2%+")
  (my-volume-display))
(defun my-volume-down ()
  (interactive)
  (start-process-shell-command "amixer set Master 2%-" nil "amixer set Master 2%-")
  (my-volume-display))
(defun my-volume-display ()
  (interactive)
  (message "Vol: %d%%" (string-to-number (shell-command-to-string "amixer get Master | grep -Eo -m 1 '[[:digit:]]+%'"))))
(global-set-key (kbd "s-a") 'my-volume-up)
(global-set-key (kbd "s-z") 'my-volume-down)


(defun custom/app-launcher (command)
  "Launches an application in your PATH."
  (interactive (list (read-shell-command "$ ")))
  (start-process-shell-command command nil command))

(defun my-volume-up ()
  (interactive)
  (start-process-shell-command "amixer set Master 2%+" nil "amixer set Master 2%+")
  (my-volume-display))
(defun my-volume-down ()
  (interactive)
  (start-process-shell-command "amixer set Master 2%-" nil "amixer set Master 2%-")
  (my-volume-display))
(defun my-volume-display ()
  (interactive)
  (message "Vol: %d%%" (string-to-number (shell-command-to-string "amixer get Master | grep -Eo -m 1 '[[:digit:]]+%'"))))
(defun my-toggle-touchpad ()
  (interactive)
  (let ((state			; "0" or "1"
         (with-temp-buffer
           (insert (shell-command-to-string "xinput --list-props \"SynPS/2 Synaptics TouchPad\""))
           (beginning-of-buffer)
           (search-forward "Enabled")
           (end-of-line)
           (push-mark)
           (backward-char)
           (buffer-substring-no-properties (point) (mark)))))
    (cond ((string= state "0")
           (shell-command "xinput set-prop \"SynPS/2 Synaptics TouchPad\" \"Device Enabled\" 1")
           (message "Synaptic touchpad enabled."))
          ((string= state "1")
           (shell-command "xinput set-prop \"SynPS/2 Synaptics TouchPad\" \"Device Enabled\" 0")
           (message "Synaptic touchpad disabled."))
          (t (message "Problem with the shell command.")))
    (kill-buffer "*Shell Command Output*")))

(defun my-display-brightness-percentage ()
  (interactive)
  (message "Screen Brightness: %d%%" (string-to-number (shell-command-to-string "xbacklight -get"))))
(defun my-brightness-up ()
  (interactive)
  (start-process-shell-command "xbacklight -inc 2" nil "xbacklight -inc 2")
  (my-display-brightness-percentage))
(defun my-brightness-down ()
  (interactive)
  (start-process-shell-command "xbacklight -dec 2" nil "xbacklight -dec 2")
  (my-display-brightness-percentage))
(defun my-screenshot (num)
  (interactive "p")
  (if (= num 4)
      (start-process-shell-command "screenshot-alternative.sh" nil "screenshot-alternative.sh")
    (start-process-shell-command "screenshot.sh" nil "screenshot.sh")))
(defun my-wifi-restart ()
  (interactive)
  (start-process-shell-command "nmcli radio wifi off && sleep 5 && nmcli radio wifi on" "*restart wifi*" "nmcli radio wifi off && sleep 5 && nmcli radio wifi on"))

;; Desktop Environment-like keybindings
(global-set-key (kbd "s-=") 'my-brightness-up)
(global-set-key (kbd "s--") 'my-brightness-down)
(global-set-key (kbd "<print>") 'my-screenshot)

(use-package exwm
    ;; :disabled t
    :straight t
    :init
    :config
    (require 'exwm)
    (require 'exwm-config)
    (require 'exwm-systemtray)
    (exwm-config-default)
    (exwm-systemtray-enable)

    ;; EXWM enables `ido-mode', for some reason
    (ido-mode -1)
    
    ;; ;; Android Emulator is wrongfully detected to be able to work in tiling mode
    ;; (setq exwm-manage-configurations '(((string-match-p "^ ?Android Emulator -" exwm-title)
    ;; 					floating t)))

    ;; All buffers created in EXWM mode are named "*EXWM*". You may want to
    ;; change it in `exwm-update-class-hook' and `exwm-update-title-hook', which
    ;; are run when a new X window class name or title is available.  Here's
    ;; some advice on this topic:
    ;; + Always use `exwm-workspace-rename-buffer` to avoid naming conflict.
    ;; + For applications with multiple windows (e.g. GIMP), the class names of
					;    all windows are probably the same.  Using window titles for them makes
    ;;   more sense.
    ;; In the following example, we use class names for all windows except for
    ;; Java applications and GIMP.
    (add-hook 'exwm-update-class-hook
              (lambda ()
		(unless (or (string-prefix-p "sun-awt-X11-" exwm-instance-name)
                            (string= "gimp" exwm-instance-name))
		  (exwm-workspace-rename-buffer exwm-class-name))))
    (add-hook 'exwm-update-title-hook
              (lambda ()
		(when (or (not exwm-instance-name)
			  (string-prefix-p "sun-awt-X11-" exwm-instance-name)
			  (string= "gimp" exwm-instance-name))
		  (exwm-workspace-rename-buffer exwm-title))))

    ;; Avoid hanging the window manager
    (global-unset-key (kbd "C-z"))

    ;; For `line-mode', `char-mode' and even normal Emacs buffers
    ;; Here all the "s-X" keys can be inserted
    (setq exwm-input-global-keys
	  `(([?\s-r] . exwm-input-toggle-keyboard)
            ([?\s-w] . exwm-workspace-switch)
            ([?\s-&] . (lambda (command)
			 (interactive (list (read-shell-command "$ ")))
			 (start-process-shell-command command nil command)))
            ([?\s-a] . my-volume-up)
            ([?\s-z] . my-volume-down)
            ([M-tab] . my-switch-other-buffer)
	    ([?\s-h] . windmove-left)
	    ([?\s-l] . windmove-right)
	    ([?\s-j] . windmove-down)
	    ([?\s-k] . windmove-up)
	    ([?\s-1] . delete-other-windows)
	    ([?\s-0] . delete-window)
	    ([?\s-2] . split-window-below)
	    ([?\s-3] . split-window-right)
	    ([?\s-n] . next-buffer)
	    ([?\s-p] . previous-buffer)
	    ([?\s-e] . emms)))

    ;; Keys only used in `line-mode' that are passed to Emacs
    ;; Defaults: "C-x" "C-u" "C-h" "M-x" "M-`" "M-&" "M-:"
    ;; (push ?\s-f exwm-input-prefix-keys)
    
    ;; Make all "s-X" in `line-mode' be passed to Emacs
    (defun pass-super-through (event)
      (memq 'super (event-modifiers event)))
    (advice-add 'exwm-input--event-passthrough-p
		:after-until #'pass-super-through)
    
    ;; Simulation keys only used in `line-mode' (which corresponds to `exwm-mode-map')
    (setq exwm-input-simulation-keys
	  '(([?\C-b] . [left])
            ([?\C-f] . [right])
            ([?\C-p] . [up])
            ([?\C-n] . [down])
            ([?\C-a] . [home])
            ([?\C-e] . [end])
            ([?\M-v] . [prior])
            ([?\C-v] . [next])
            ([?\C-d] . [delete])
            ([?\C-k] . [S-end delete])
            ([?\C-m] . [return])
            ([?\C-i] . [tab])
            ([?\C-g] . [escape])
            ([?\M-w] . [?\C-c])
            ([?\C-w] . [?\C-x])
            ([?\C-y] . [?\C-v])
            ([?\C-s] . [?\C-f])
            ([?\C-\_] . [?\C-z])
            ([?\M-f] . [C-right])
            ([?\M-b] . [C-left])
            ([?\M-d] . [C-S-right delete])
            ([?\C-0] . [C-w])	   ; e.g. to close Firefox/Thunar tabs
            ;; ([?\C-S-f] . [S-right])
            ;;(,(kbd "C-[") . [escape])
            ;;(,(kbd "C-S-DEL") . [home S-end delete])
            ;;(,(kbd "M-DEL") . [S-C-left delete])
            )))

(use-package exwm-edit
    :straight t
    :after exwm
    :config
    (defun exwm-edit-compose-hook-for-prose ()
      (org-mode)
      (auto-fill-mode -1)
      (visual-line-mode))
    (add-hook 'exwm-edit-compose-hook 'exwm-edit-compose-hook-for-prose))

(provide 'setup-exwm)
;;; setup-exwm.el ends here

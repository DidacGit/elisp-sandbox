;;; setup-completion-and-search.el --- My completion configuration

;;; Commentary:

;; My Ivy Mode configuration

;;; Code:

(require 'use-package)

(use-package minibuffer
    :hook (after-init . minibuffer-depth-indicate-mode) ; recursion depth
    :config
    ;; TODO: make a toggle between completion styles (use Prot for inspiration)
    (setq enable-recursive-minibuffers t)
    ;; (setq completion-styles '(multisubstring))
    (setq completion-auto-help nil)
    (setq completion-flex-nospace nil)
    (setq read-file-name-completion-ignore-case t
	  read-buffer-completion-ignore-case t)
    (setq completion-pcm-complete-word-inserts-delimiters t)
    (setq completion-show-help nil)
    ;; (setq resize-mini-windows t)
    (defun backward-kill-filepath ()
      "Kill the current file path segment backwards."
      (interactive)
      (when (char-equal (char-before) ?/)
        (delete-backward-char 1))
      (if (search-backward "/" nil t)
	  (forward-char)
	(beginning-of-line))
      (kill-line))
    :bind (:map minibuffer-local-filename-completion-map
		("M-DEL" . backward-kill-filepath)))

(use-package icomplete
    :hook (after-init . icomplete-mode)
    :bind (:map icomplete-minibuffer-map
		;; TODO: Configure depending on the minibuffer keymap
		("SPC" . self-insert-command) ; To use the `orderless' completion style
		;; ("C-j" . minibuffer-complete-and-exit) ; Submit string
		;; ("RET" . icomplete-force-complete-and-exit) ; Select candidate
		("C-o" . icomplete-force-complete) ; Insert candidate
		))

(use-package icomplete-vertical
    :straight t
    :after icomplete
    ;; It needs to be loaded after icomplete gets loaded:
    :hook (icomplete-mode . icomplete-vertical-mode)
    :bind (:map icomplete-minibuffer-map
		("C-n" . icomplete-forward-completions)
		("C-p" . icomplete-backward-completions)))

(use-package orderless
    :straight t
    :config
    (setq completion-styles '(orderless)))

(use-package savehist
	     :hook (after-init . savehist-mode)
	     :config
	     (setq history-length 30000)
	     (setq history-delete-duplicates nil))

(use-package isearch
    :config
  (defun isearch-other-end ()
    "End current search in the opposite side of the match.
Particularly useful when the match does not fall within the
confines of word boundaries (e.g. multiple words)."
    (interactive)
    (isearch-done)
    (when isearch-other-end
      (goto-char isearch-other-end)))

  (defun isearch-obliviate ()
    "Cancel the search and return to starting point.
The difference between this function and `isearch-cancel' is that
this one does not signal `quit'."
    (interactive)
    (isearch-exit)
    (pop-to-mark-command))
  
  (setq search-whitespace-regexp ".*?")
  ;; Display the count of matches
  (setq isearch-lazy-count t)
  (setq lazy-count-prefix-format "(%s/%s) ")
  :bind (("M-s M-s" . multi-isearch-buffers)
	 :map isearch-mode-map
	 ("RET" . isearch-other-end)
	 ("M-RET" . isearch-exit)
	 ("C-g" . isearch-obliviate)))

(provide 'setup-completion-and-search)
;;; setup-completion-and-search.el ends here
